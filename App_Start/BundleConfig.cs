﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace Localization.Bundles
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            //-------------------JavaScript-------------------

            bundles.Add(new ScriptBundle("~/JavaScript/ref").Include(
                "~/Scripts/ref/jquery-1.9.1.min.js"
                )
            );
        }
    }
}